# NeverEnding

[![Build Status](https://app.bitrise.io/app/614840e3aba7b16d/status.svg?token=qmlxdxu8LWcSAdHNTf9Wpg&branch=master)](https://app.bitrise.io/app/614840e3aba7b16d)


### Technical Details:

#### a) Design Patterns: 
MVC, Dependency Injection

#### b) Dependency Manager: CocoaPods
 1. Alamofire - REST API calls

 2. SwiftyJSON - Object Mapping to JSON

 3. SDWebImage - For Image Caching

#### c) Unit Tests 
Using XCTest Framework (Partially for the Controller, Model, Service and Utility Modules and UI Input Validations). Test Coverage details attached.

I am facing some issues with UITest in xcode 9.4, even on an empty project xcode is failing to run the example test. I will try to download previous version of xcode and try with it and update the same.

#### d) Xcode: 9.4, Swift: 3

#### e) Image Source
For the image source, I have used Pixaby (https://pixabay.com/api/docs/). And it supports pagination. For each request, I have kept the perPage as 20 (configurable). And the images are cached.
To limit memory usage, I have used a parameter (configurable) for a maximum number of images at a time, which is set as 500.
Now, when the user scrolls and is about to reach the end of the list, say 480th record. The first 100 record from the beginning (again configurable), gets freed from memory. Even the cache for these images is cleared.

#### f) Future Enhancements Proposal: 
- For the time being, I have kept the pagination only at the bottom of the grid. But later on, it can be a two-sided pagination. Also I will use prefetch data source in collectionview which can fetch cells not yet displayed and cache the images so scrolling can be pretty smooth and without the placeholder image.
- I can display images in a view controller and also give 3d peak and pop feature. view controller will also have sharesheet to share image
- Gallery could have different view style now its just grid later we can add card style.

#### g) Continous Integration: Bitrise
I wanted to use Buddybuild as I am used to it but after Apple acquisition they have removed support for free plans.
But with Bitrise can also upload the build to AppStore with each push made to Master branch. I haven't configured it yet


#### h) Installation:

```
open terminal
gem install cocoapods
git clone https://gitlab.com/kamalwadhwa/NeverEnding.git
cd NeverEnding  
pod install 
open NeverEnding.xcworkspace

```

